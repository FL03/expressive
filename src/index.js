var express = require('express');
var port = 8999;

var app = express();


function homepage(req, res, next) {
    res.send("Hello World")
}


app.get("/", homepage)

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})